package exercises03.game

import scala.annotation.tailrec
import scala.util.Random

object Starter extends App {
  def unsafeRun(game: Game, controller: GameController): Unit = {
    @tailrec
    def recursive(completed: Boolean): Unit =
      if (completed) ()
      else {
        controller.askNumber()
        val state = game.parseState(controller.nextLine())
        game.action(state, game.number)(controller)
        recursive(completed = game.completed(state))
      }

    recursive(completed = false)
  }

  val number: Int = Random.nextInt(10) + 1
  unsafeRun(new Game(number), GameController.live)
}
