package exercises03.game

class Game(val number: Int) {
  def parseState(input: String): State = ???

  def action(state: State, number: Int): GameController => Unit = ???

  def completed(state: State): Boolean = ???
}
